import 'react-native-gesture-handler';
import React, { useEffect, useState } from 'react';
import {
  Platform,
  StyleSheet,
  ScrollView,
  View,
  Image,
  Text,
  StatusBar,
  TouchableOpacity
} from 'react-native';

import RNBootSplash from "react-native-bootsplash";
import StackScreen from './stackScreen/mainStack';

// D:\>cd MobileAppDev\ProXpr
// npx react-native run-android

export default function App () {

  useEffect(() => {
    RNBootSplash.hide({ fade: true });
  }, []);

  return(
    <StackScreen />
  )
}