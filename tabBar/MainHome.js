import 'react-native-gesture-handler';
import React, {useEffect, useState} from 'react';
import {
  Platform,
  StyleSheet,
  ScrollView,
  View,
  Image,
  Text,
  StatusBar,
  TouchableOpacity,
} from 'react-native';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import {Background} from '../component/Background';
import Logo from '../component/Logo';
import Header from '../component/Header';
// import Button from '../component/Button';
import Paragraph from '../component/Paragraph';
import TextInput from '../component/TextInput';
import {theme} from '../assets/theme';
import {Button} from 'react-native-paper';

// Dashboard Appbar
function Appbar({navigation}) {
  return (
    <View style={styles.appbar}>
      <View style={styles.titleKelasKoding}>
        <Image
          style={styles.imageTitle}
          source={require('../assets/img/title.png')}
        />
      </View>
      <TouchableOpacity
        onPress={() => navigation.openDrawer()}
        style={styles.menuBurger}>
        <Image
          style={styles.image}
          source={require('../assets/img/menu.png')}
        />
      </TouchableOpacity>
    </View>
  );
}

// Dashboard Content
function Content({props, title, onPress, children, navigation}) {
  return (
    <View style={styles.contentContainer}>
      <Text style={styles.contentTitle}>{title}</Text>
      <ScrollView
        horizontal
        style={styles.scrollView}
        contentContainerStyle={{alignItems: 'center'}}>
        {children}
      </ScrollView>
      <Button
        mode="contained"
        style={styles.button}
        labelStyle={{fontSize: 12}}
        onPress={onPress}>
        See all
      </Button>
    </View>
  );
}

// Courses item
function Courses({props, navigation, onPress, source, style, courseName}) {
  return (
    <TouchableOpacity style={styles.courses} onPress={onPress}>
      <View style={{alignItems: 'center', borderWidth: 0.5, borderColor: '#bdbdbd', borderRadius: 5}}>
        <Image
          source={source}
          style={{width: 50, height: 50}}
          resizeMode="contain"
        />
        <Text style={style}>{courseName}</Text>
      </View>
    </TouchableOpacity>
  );
}

// Dashboard Home Screen
export default function HomeScreen({navigation}) {
  return (
    <Background>
      <StatusBar barStyle="dark-content" backgroundColor="#ffffff" />
      <Appbar navigation={navigation} />
      <Content
        title="Top Free Course"
        navigation={navigation}
        onPress={() => console.log('noice')}>
        {/* tambah course/event dengan component Courses */}
        <Courses
          source={require('../assets/img/html.png')}
          courseName="HTML"
          onPress={() => console.log('nice')}
        />
        <Courses
          source={require('../assets/img/html.png')}
          courseName="HTML"
          onPress={() => console.log('nice')}
        />
      </Content>

      <Content title="Top Paid Course" navigation={navigation}>
        {/* tambah course/event dengan component Courses */}
        <Courses
          source={require('../assets/img/python.png')}
          courseName="Python"
          onPress={() => console.log("Good job, it's work")}
        />
        <Courses
          source={require('../assets/img/javascript.png')}
          courseName="Javascript"
          onPress={() => console.log('Sip, Mantap')}
        />
      </Content>
      <Content title="Soon Webinar Event" navigation={navigation}>
        {/* tambah course/event dengan component Courses */}
      </Content>
      <Content title="Soon Live Event" navigation={navigation}>
        {/* tambah course/event dengan component Courses */}
      </Content>

      {/* <Header>Let’s start</Header>
      <Paragraph>
        Welcome to the dashboard menu KelasKoding, just Good Luck & Have Fun.
      </Paragraph> */}
      {/* 
      <Button mode="outlined" onPress={() => navigation.navigate('Welcome')}>
        Logout
      </Button> 
      */}
    </Background>
  );
}

const styles = StyleSheet.create({
  appbar: {
    flex: 0.5,
    width: '100%',
    borderBottomWidth: 0.5,
    borderBottomColor: '#bdbdbd',
  },
  titleKelasKoding: {
    position: 'absolute',
    top: 5 + getStatusBarHeight(),
    left: 0,
  },
  imageTitle: {
    width: 160,
    height: 30,
  },
  menuBurger: {
    position: 'absolute',
    top: 5 + getStatusBarHeight(),
    right: 5,
    // backgroundColor: '#f3f3f3',
  },
  image: {
    width: 22,
    height: 22,
  },
  contentContainer: {
    flex: 1,
    alignSelf: 'flex-start',
    width: '100%',
    // borderWidth: 0.5,
  },
  contentTitle: {
    fontSize: 24,
    fontWeight: 'bold',
    color: theme.colors.primary,
    marginVertical: 8,
  },
  scrollView: {
    borderWidth: 1,
    borderRadius: 5,
    borderColor: '#bdbdbd',
    height: 75,
  },
  courses: {
    margin: 8,
  },
  button: {
    backgroundColor: theme.colors.secondary,
    height: 30,
    alignSelf: 'flex-end',
    width: 100,
    marginTop: 5,
  },
});
