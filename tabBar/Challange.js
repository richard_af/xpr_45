import 'react-native-gesture-handler';
import React, { useEffect, useState } from 'react';
import {
  Platform,
  StyleSheet,
  ScrollView,
  View,
  Image,
  Text,
  StatusBar,
  TouchableOpacity
} from 'react-native';

import { Background } from '../component/Background';
import Logo from '../component/Logo';
import Header from '../component/Header';
import Button from '../component/Button';
import Paragraph from '../component/Paragraph';
import TextInput from '../component/TextInput';
import { theme } from '../assets/theme';

export default function ChallangeScreen ({ navigation }) {
  return(
    <Background>
      <Logo />
      <Header>Let’s start</Header>
      <Paragraph>
        Welcome to the Challange menu KelasKoding.
      </Paragraph>
    </Background>
  )
}