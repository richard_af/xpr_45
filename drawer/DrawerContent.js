import React, { useEffect, useState } from 'react';
import {
  Platform,
  StyleSheet,
  ScrollView,
  View,
  Image,
  Text,
  StatusBar,
  TouchableOpacity,
  Linking,
} from 'react-native';

import {
  Avatar,
  Title,
  Caption,
  Paragraph,
  Drawer,
  TouchableRipple,
  Switch,
} from 'react-native-paper';

import { 
  createDrawerNavigator,
  DrawerItem,
  DrawerContentScrollView,
} from '@react-navigation/drawer';

export function DrawerContent({ props, navigation }) {

  return(
    <View style={{flex: 1,}}>
      <DrawerContentScrollView { ...props }>
        <View style={styles.drawerContent}>
          
          <View style={styles.userInfoSection}>
            <View style={{flexDirection: 'row', marginVertical: 15, justifyContent: 'center'}}>
              <Avatar.Image 
                source={require('../assets/img/testAvatar1.jpg')}
                size={110}
              />
              
              {/* <View style={{flexDirection: 'column', marginLeft: 15}}>
                <Title style={styles.title}>Ron Austinne</Title>
                <Caption style={styles.caption}>id: 1173254233</Caption>
              </View> */}
            </View>
            
            {/* <View style={styles.row}>
              <View style={styles.section}>
                <Paragraph style={styles.paragraph, styles.profileInfo}>12</Paragraph>
                <Caption style={styles.caption}>Friends</Caption>
              </View>
              <View style={styles.section}>
                <Paragraph style={styles.paragraph, styles.profileInfo}>3</Paragraph>
                <Caption style={styles.caption}>Master Class</Caption>
              </View>
            </View> */}
          </View>
        
          <Drawer.Section style={styles.drawerSection}>
            <DrawerItem 
              style={{
                borderBottomColor: '#E2E2E2',
                borderBottomWidth: 1,
                borderLeftColor: '#E2E2E2',
                borderLeftWidth: 1,
                borderRightColor: '#E2E2E2',
                borderRightWidth: 1,
                borderBottomLeftRadius: 7,
                borderBottomRightRadius: 7,
                borderTopColor: '#ededed',
                borderTopWidth: 0.5,
                borderTopLeftRadius: 7,
                borderTopRightRadius: 7,
              }}
              icon={({ focused, color, size }) => (
                <Image
                  source={require('../assets/img/homeDrawer.png')}
                  style={{ height: 15, width: 15 }}
                  resizeMode="contain"
                />
              )}
              label='Home'
              onPress={() => navigation.navigate('Home')}
            />

            <DrawerItem 
              style={{
                borderBottomColor: '#E2E2E2',
                borderBottomWidth: 1,
                borderLeftColor: '#E2E2E2',
                borderLeftWidth: 1,
                borderRightColor: '#E2E2E2',
                borderRightWidth: 1,
                borderBottomLeftRadius: 7,
                borderBottomRightRadius: 7,
                borderTopColor: '#ededed',
                borderTopWidth: 0.5,
                borderTopLeftRadius: 7,
                borderTopRightRadius: 7,
              }}
              icon={({ focused, color, size }) => (
                <Image
                  source={require('../assets/img/profileDrawer.png')}
                  style={{ height: 15, width: 15 }}
                  resizeMode="contain"
                />
              )}
              label='Profile'
              onPress={() => navigation.navigate('Profile')}
            />

            <DrawerItem 
              style={{
                borderBottomColor: '#E2E2E2',
                borderBottomWidth: 1,
                borderLeftColor: '#E2E2E2',
                borderLeftWidth: 1,
                borderRightColor: '#E2E2E2',
                borderRightWidth: 1,
                borderBottomLeftRadius: 7,
                borderBottomRightRadius: 7,
                borderTopColor: '#ededed',
                borderTopWidth: 0.5,
                borderTopLeftRadius: 7,
                borderTopRightRadius: 7,
              }}
              icon={({ focused, color, size }) => (
                <Image
                  source={require('../assets/img/eventDrawer.png')}
                  style={{ height: 15, width: 15 }}
                  resizeMode="contain"
                />
              )}
              label='Event Saya'
              onPress={() => navigation.navigate('My Event')}
            /> 

            <DrawerItem 
              style={{
                borderBottomColor: '#E2E2E2',
                borderBottomWidth: 1,
                borderLeftColor: '#E2E2E2',
                borderLeftWidth: 1,
                borderRightColor: '#E2E2E2',
                borderRightWidth: 1,
                borderBottomLeftRadius: 7,
                borderBottomRightRadius: 7,
                borderTopColor: '#ededed',
                borderTopWidth: 0.5,
                borderTopLeftRadius: 7,
                borderTopRightRadius: 7,
              }}
              icon={({ focused, color, size }) => (
                <Image
                  source={require('../assets/img/courseDrawer.png')}
                  style={{ height: 15, width: 15 }}
                  resizeMode="contain"
                />
              )}
              label='Course Saya'
              onPress={() => navigation.navigate('My Course')}
            /> 

            <DrawerItem 
              style={{
                borderBottomColor: '#E2E2E2',
                borderBottomWidth: 1,
                borderLeftColor: '#E2E2E2',
                borderLeftWidth: 1,
                borderRightColor: '#E2E2E2',
                borderRightWidth: 1,
                borderBottomLeftRadius: 7,
                borderBottomRightRadius: 7,
                borderTopColor: '#ededed',
                borderTopWidth: 0.5,
                borderTopLeftRadius: 7,
                borderTopRightRadius: 7,
              }}
              icon={({ focused, color, size }) => (
                <Image
                  source={require('../assets/img/challangeDrawer.png')}
                  style={{ height: 15, width: 15 }}
                  resizeMode="contain"
                />
              )}
              label='Challange Saya'
              onPress={() => navigation.navigate('My Challange')}
            /> 

            <DrawerItem 
              style={{
                borderBottomColor: '#E2E2E2',
                borderBottomWidth: 1,
                borderLeftColor: '#E2E2E2',
                borderLeftWidth: 1,
                borderRightColor: '#E2E2E2',
                borderRightWidth: 1,
                borderBottomLeftRadius: 7,
                borderBottomRightRadius: 7,
                borderTopColor: '#ededed',
                borderTopWidth: 0.5,
                borderTopLeftRadius: 7,
                borderTopRightRadius: 7,
              }}
              icon={({ focused, color, size }) => (
                <Image
                  source={require('../assets/img/historyDrawer.png')}
                  style={{ height: 15, width: 15 }}
                  resizeMode="contain"
                />
              )}
              label='Riwayat Transaksi'
              onPress={() => navigation.navigate('Transaksi')}
            /> 

            <DrawerItem 
              style={{
                borderBottomColor: '#E2E2E2',
                borderBottomWidth: 1,
                borderLeftColor: '#E2E2E2',
                borderLeftWidth: 1,
                borderRightColor: '#E2E2E2',
                borderRightWidth: 1,
                borderBottomLeftRadius: 7,
                borderBottomRightRadius: 7,
                borderTopColor: '#ededed',
                borderTopWidth: 0.5,
                borderTopLeftRadius: 7,
                borderTopRightRadius: 7,
              }}
              icon={({ focused, color, size }) => (
                <Image
                  source={require('../assets/img/notifDrawer.png')}
                  style={{ height: 15, width: 15 }}
                  resizeMode="contain"
                />
              )}
              label='Notifikasi'
              onPress={() => navigation.navigate('Notifikasi')}
            />

          </Drawer.Section>

        </View>
      </DrawerContentScrollView>

      <Drawer.Section style={styles.bottomDrawerSection}>
        <DrawerItem
          style={{
            borderBottomColor: '#E2E2E2',
            borderBottomWidth: 1,
            borderLeftColor: '#E2E2E2',
            borderLeftWidth: 1,
            borderRightColor: '#E2E2E2',
            borderRightWidth: 1,
            borderBottomLeftRadius: 7,
            borderBottomRightRadius: 7,
            borderTopColor: '#ededed',
            borderTopWidth: 0.5,
            borderTopLeftRadius: 7,
            borderTopRightRadius: 7,
          }}
          icon={({ focused, color, size }) => (
            <Image
              source={require('../assets/img/supportDrawer.png')}
              style={{ height: 15, width: 15 }}
              resizeMode="contain"
            />
          )}
          label="Contact"
          onPress={() => Linking.openURL('https://www.google.com/')}
        />
        <DrawerItem 
          style={{
            borderBottomColor: '#E2E2E2',
            borderBottomWidth: 1,
            borderLeftColor: '#E2E2E2',
            borderLeftWidth: 1,
            borderRightColor: '#E2E2E2',
            borderRightWidth: 1,
            borderBottomLeftRadius: 7,
            borderBottomRightRadius: 7,
            borderTopColor: '#ededed',
            borderTopWidth: 0.5,
            borderTopLeftRadius: 7,
            borderTopRightRadius: 7,
          }}
          icon={({ focused, color, size }) => (
            <Image
              source={require('../assets/img/signOut.png')}
              style={{ height: 15, width: 15 }}
              resizeMode="contain"
            />
          )}
          label='Sign Out'
          onPress={() => navigation.navigate('Login')}
        />
      </Drawer.Section>
    </View>
  )
}

const styles = StyleSheet.create({
  drawerContent: {
    flex: 1,
  },
  userInfoSection: {
    paddingLeft: 20,
  },
  title: {
    fontSize: 16,
    marginTop: 3,
    fontWeight: 'bold',
  },
  caption: {
    fontSize: 12,
    lineHeight: 14,
  },
  row: {
    marginTop: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },
  section: {
    flexDirection: 'row',
    alignItems: 'center',
    marginRight: 15,
  },
  paragraph: {
    fontWeight: 'bold',
    marginRight: 15,
  },
  profileInfo: {
    marginRight: 5,
    flexDirection: 'row',
    alignItems: 'center',
  },
  drawerSection: {
    marginTop: 15,
  },
  bottomDrawerSection: {
    marginBottom: 1,
    borderTopColor: '#f4f4f4',
    borderTopWidth: 1,
  },
  preference: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 12,
    paddingHorizontal: 16,
  },
});