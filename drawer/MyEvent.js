import 'react-native-gesture-handler';
import React, { useEffect, useState } from 'react';
import {
  Platform,
  StyleSheet,
  ScrollView,
  View,
  Image,
  Text,
  StatusBar,
  TouchableOpacity
} from 'react-native';
import { getStatusBarHeight } from 'react-native-status-bar-height';

import { Background } from '../component/Background';
import Logo from '../component/Logo';
import Header from '../component/Header';
import Button from '../component/Button';
import Paragraph from '../component/Paragraph';
import TextInput from '../component/TextInput';
import { theme } from '../assets/theme';

export default function EventSaya ({ navigation }) {
  return(
    <Background>
      <TouchableOpacity onPress={() => navigation.navigate('Home')} style={styles.backButton}>
        <Image style={styles.imageBackButton} source={require('../assets/img/arrow_back.png')} />
      </TouchableOpacity>
      <TouchableOpacity onPress={() => navigation.openDrawer()} style={styles.menuBurger}>
        <Image style={styles.image} source={require('../assets/img/menu.png')} />
      </TouchableOpacity>
      <Header>Your Event</Header>
      <Paragraph>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. 
        Eos minus sequi earum quaerat incidunt quisquam non neque, 
        natus perferendis iusto repellendus necessitatibus ullam porro officia reiciendis. 
        Magni, omnis inventore. Nemo!
      </Paragraph>
    </Background>
  )
}

const styles = StyleSheet.create({
  backButton: {
    position: 'absolute',
    top: 5 + getStatusBarHeight(),
    left: 0,
  },
  imageTitle: {
    width: 160,
    height: 30,
  },
  menuBurger: {
    position: 'absolute',
    top: 5 + getStatusBarHeight(),
    right: 5,
    // backgroundColor: '#f3f3f3',
  },
  image: {
    width: 22,
    height: 22,
  },
  imageBackButton: {
    width: 25,
    height: 25,
  },
})