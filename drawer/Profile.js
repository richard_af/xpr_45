import 'react-native-gesture-handler';
import React, { useEffect, useState } from 'react';
import {
  Platform,
  SafeAreaView,
  ImageBackground,
  StyleSheet,
  ScrollView,
  View,
  Image,
  Text,
  StatusBar,
  TouchableOpacity,
  TextInput
} from 'react-native';

import {
  Avatar,
  Title,
  Caption,
  Paragraph,
  Drawer,
  TouchableRipple,
  Switch,
} from 'react-native-paper';
import { getStatusBarHeight } from 'react-native-status-bar-height';

import { Background } from '../component/Background';
import Logo from '../component/Logo';
import Header from '../component/Header';
import Button from '../component/Button';
// import Paragraph from '../component/Paragraph';
import { TextInputFlat } from '../component/TextInput';
import { theme } from '../assets/theme';

export default function ProfileScreen ({ navigation }) {
  return(
    <Background>
      <TouchableOpacity onPress={() => navigation.navigate('Home')} style={styles.backButton}>
        <Image style={styles.imageBackButton} source={require('../assets/img/arrow_back.png')} />
      </TouchableOpacity>
      <TouchableOpacity onPress={() => navigation.openDrawer()} style={styles.menuBurger}>
        <Image style={styles.image} source={require('../assets/img/menu.png')} />
      </TouchableOpacity>
      <ScrollView style={[styles.container, {marginTop: 10 + getStatusBarHeight(),}]}>
        <View style={{margin: 20}}>
          <View style={{alignItems: 'center'}}>
            <TouchableOpacity onPress={() => {}}>
              <View style={{
                height: 100,
                width: 100,
                borderRadius: 60,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
                <ImageBackground 
                  source={require('../assets/img/testAvatar1.jpg')}
                  style={{width: 100, height: 100,}}
                  imageStyle={{borderRadius: 60}}
                >
                  <View style={{
                    flex: 1,
                    justifyContent: 'flex-end',
                    alignItems: 'flex-end',
                  }}>
                    <Image 
                      source={require('../assets/img/camera.png')}
                      style={{
                        width: 35, 
                        height: 35,
                        opacity: 0.9,
                        alignItems: 'center',
                        borderWidth: 1,
                        borderColor: '#009FFF',
                        borderRadius: 10,
                      }}
                    />
                  </View>
                </ImageBackground>
              </View>
            </TouchableOpacity>
            <Text style={{ marginTop: 10, fontSize: 20, fontWeight: 'bold'}}>Ron Austinne</Text>
          </View>
        </View>
        <View style={styles.action}>
          <Image
            source={require('../assets/img/email.png')}
            style={{ height: 15, width: 15, position: 'absolute', marginTop: 15,}}
          />
          <TextInput
            placeholder="Your Email"
            placeholderTextColor= {theme.colors.placeholder}
            autoCorrect={false}
            style={styles.textInput}
            keyboardType='email-address'
          />
        </View>

        <View style={styles.action}>
          <Image
            source={require('../assets/img/userProfile.png')}
            style={{ height: 15, width: 15, position: 'absolute', marginTop: 15,}}
          />
          <TextInput
            placeholder="Your Name"
            placeholderTextColor= {theme.colors.placeholder}
            autoCorrect={false}
            style={styles.textInput}
          />
        </View>

        <View style={styles.action}>
          <Image
            source={require('../assets/img/lock.png')}
            style={{ height: 15, width: 15, position: 'absolute', marginTop: 15,}}
          />
          <TextInput
            placeholder="Your Password"
            placeholderTextColor= {theme.colors.placeholder}
            autoCorrect={false}
            style={styles.textInput}
            secureTextEntry
          />
        </View>

        <View style={styles.action}>
          <Image
            source={require('../assets/img/callNumber.png')}
            style={{ height: 15, width: 15, position: 'absolute', marginTop: 15,}}
          />
          <TextInput
            placeholder="Your Phone Number"
            placeholderTextColor= {theme.colors.placeholder}
            autoCorrect={false}
            style={styles.textInput}
            keyboardType='numeric'
          />
        </View>
        
        <View style={{marginTop: 30, width: '50%', alignSelf: 'center'}}>
          <Button
            mode="outlined"
            onPress={() => {}}
          >
            Save
          </Button>
        </View>
      </ScrollView>
    </Background>
  )
}

const styles = StyleSheet.create({
  background: {
    flex: 1,
    width: '100%',
  },
  backButton: {
    position: 'absolute',
    top: 5 + getStatusBarHeight(),
    left: 0,
  },
  imageTitle: {
    width: 160,
    height: 30,
  },
  menuBurger: {
    position: 'absolute',
    top: 5 + getStatusBarHeight(),
    right: 5,
    // backgroundColor: '#f3f3f3',
  },
  image: {
    width: 22,
    height: 22,
  },
  imageBackButton: {
    width: 25,
    height: 25,
  },

  container: {
    flex: 1,
    width: '100%',
  },
  commandButton: {
    padding: 15,
    borderRadius: 10,
    backgroundColor: '#FF6347',
    alignItems: 'center',
    marginTop: 10,
  },
  panel: {
    padding: 20,
    backgroundColor: '#FFF',
    paddingTop: 20,
    // borderTopLeftRadius: 20,
    // borderTopRightRadius: 20,
    // shadowColor: '#000',
    // shadowOffset: {width: 0, height: 0},
    // shadowRadius: 5,
    // shadowOpacity: 0.4,
  },
  header: {
    backgroundColor: '#FFF',
    shadowColor: '#333',
    shadowOffset: {width: -1, height: -3},
    shadowRadius: 2,
    shadowOpacity: 0.4,
    paddingTop: 20,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    // elevation: 5,
  },
  panelHeader: {
    width: 40,
    height: 8,
    borderRadius: 4,
    backgroundColor: '#00000040',
    marginBottom: 10,
  },
  panelTitle: {
    fontSize: 27,
    height: 35,
  },
  panelSubtitle: {
    fontSize: 14,
    color: 'gray',
    height: 30,
    marginBottom: 10,
  },
  panelButton: {
    padding: 13,
    borderRadius: 10,
    backgroundColor: '#FF6347',
    alignItems: 'center',
    marginVertical: 7,
  },
  panelButtonTitle: {
    fontSize: 17,
    fontWeight: 'bold',
    color: 'white',
  },
  action: {
    flexDirection: 'row',
    marginTop: 10,
    marginBottom: 5,
    borderBottomWidth: 1,
    borderBottomColor: '#666666',
    paddingBottom: 2,
  },
  actionError: {
    flexDirection: 'row',
    marginTop: 10,
    marginBottom: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#FF0000',
    paddingBottom: 2,
  },
  textInput : {
    flex: 1,
    // marginTop: Platform.OS 'ios' ? 0 : -12,
    // paddingLeft: 15,
    marginLeft: 30,
    color: '#05375a',
  },
})