import React from 'react';
import {StyleSheet} from 'react-native';
import {Button as PaperButton} from 'react-native-paper';
import {theme} from '../assets/theme';

export default function Button({mode, style, children, ...props}) {
  return (
    <PaperButton
      style={[
        styles.button,
        mode === 'contained' && {backgroundColor: theme.colors.login},
        mode === 'outlined' && {backgroundColor: theme.colors.secondary},
        style,
      ]}
      labelStyle={styles.text}
      mode={mode}
      {...props}>
      {children}
    </PaperButton>
  );
}

const styles = StyleSheet.create({
  button: {
    width: '100%',
    marginTop: 10,
    marginBottom: 15,
  },
  text: {
    fontWeight: 'bold',
    fontSize: 15,
    lineHeight: 26,
    color: 'white',
  },
});
