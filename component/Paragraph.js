import React from 'react';
import { StyleSheet, Text } from 'react-native';
import { theme } from '../assets/theme';

export default function Paragraph(props) {
  return(
    <Text style={styles.text}>{ props.children }</Text>
  )
}

const styles = StyleSheet.create({
  text: {
    fontSize: 16,
    lineHeight: 26,
    color: theme.colors.secondary,
    textAlign: 'center',
    marginBottom: 14,
  },
});
