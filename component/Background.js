import React from 'react';
import {
  View,
  ImageBackground,
  StyleSheet,
  ScrollView,
  KeyboardAvoidingView,
} from 'react-native';

export function BackgroundGradient(props) {
  return(
    <ImageBackground
      source={require('../assets/img/bg.png')}
      style={styles.background}
    >
      <View style={styles.container} behavior="padding">
        { props.children }
      </View>
    </ImageBackground>
  )
}

export function Background(props) {
  return(
    <ImageBackground
      source={require('../assets/img/bgFFF.jpg')}
      style={styles.background}
    >
      <View style={styles.container} behavior="padding">
        { props.children }
      </View>
    </ImageBackground>
  )
}


const styles = StyleSheet.create({
  background: {
    flex: 1,
    width: '100%',
  },
  container: {
    flex: 1,
    padding: 10,
    width: '90%',
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
});