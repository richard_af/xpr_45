import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { theme } from '../assets/theme';

export default function Header(props) {
  return(
    <Text style={styles.header}>{ props.children }</Text>
  )
}

const styles = StyleSheet.create({
  header: {
    fontSize: 26,
    color: theme.colors.primary,
    fontWeight: 'bold',
    paddingVertical: 14,
  },
});