import { DefaultTheme } from 'react-native-paper';

export const theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: '#2271a1',
    secondary: '#414757',
    error: '#f13a59',
    login: '#1b79b3',
    sign: '#fafafa',
    statusbar: '#C9EDF3',
    surface: '#fafafa',
    placeholder: '#666666'
  },
};