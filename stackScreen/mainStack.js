import 'react-native-gesture-handler';
import React, {useEffect, useState} from 'react';
import {
  Platform,
  StyleSheet,
  ScrollView,
  View,
  Image,
  Text,
  StatusBar,
  TouchableOpacity,
} from 'react-native';
import RNBootSplash from 'react-native-bootsplash';
import {createStackNavigator} from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';
import {
  createDrawerNavigator,
  DrawerItem,
  DrawerContentScrollView,
} from '@react-navigation/drawer';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {getStatusBarHeight} from 'react-native-status-bar-height';
// import { TextInput } from 'react-native-paper';
import {BackgroundGradient} from '../component/Background';
import Logo from '../component/Logo';
import Header from '../component/Header';
import Button from '../component/Button';
import Paragraph from '../component/Paragraph';
import {TextInput} from '../component/TextInput';
import {theme} from '../assets/theme';
import {
  emailValidator,
  passwordValidator,
  nameValidator,
} from '../assets/utils';
import HomeScreen from '../tabBar/MainHome';
import CourseScreen from '../tabBar/Course';
import ListScreen from '../tabBar/List';
import ChallangeScreen from '../tabBar/Challange';
import ProfileScreen from '../drawer/Profile';
import CourseSaya from '../drawer/MyCourse';
import EventSaya from '../drawer/MyEvent';
import ChallangeSaya from '../drawer/MyChallange';
import {DrawerContent} from '../drawer/DrawerContent';
import RiwayatTransaksi from '../drawer/RiwayatTransaksi';
import Notification from '../drawer/Notif';

//WELCOME SCREEN

function WelcomeScreen({navigation}) {
  useEffect(() => {
    RNBootSplash.hide({fade: true});
  }, []);

  return (
    <BackgroundGradient>
      <StatusBar barStyle="dark-content" backgroundColor="#C9EDF3" />
      <Logo />
      <Header>KelasKoding</Header>

      <Paragraph>
        Ayo mulai perjalanan anda di dalam dunia pemrograman melalui
        KelasKoding.
      </Paragraph>
      <Button mode="contained" onPress={() => navigation.navigate('Login')}>
        Start
      </Button>
      {/* <Button
        mode="outlined"
        onPress={() => navigation.navigate('Register')}
      >
        Sign Up
      </Button> */}
    </BackgroundGradient>
  );
}

//LOGIN SCREEN

function LoginScreen({errorText, navigation}) {
  const [email, setEmail] = useState({value: '', error: ''});
  const [password, setPassword] = useState({value: '', error: ''});

  const _onLoginPressed = () => {
    const emailError = emailValidator(email.value);
    const passwordError = passwordValidator(password.value);

    if (emailError || passwordError) {
      setEmail({...email, error: emailError});
      setPassword({...password, error: passwordError});
      return;
    }

    navigation.navigate('Dashboard');
  };

  return (
    <BackgroundGradient>
      <TouchableOpacity
        onPress={() => navigation.goBack()}
        style={styles.container}>
        <Image
          style={styles.image}
          source={require('../assets/img/arrow_back.png')}
        />
      </TouchableOpacity>
      <Logo />

      <Header>Login To Your Account.</Header>

      <TextInput
        label="Email"
        returnKeyType="next"
        value={email.value}
        onChangeText={text => setEmail({value: text, error: ''})}
        error={!!email.error}
        errorText={email.error}
        autoCapitalize="none"
        autoCompleteType="email"
        textContentType="emailAddress"
        keyboardType="email-address"
      />
      {errorText ? <Text style={styles.error}>{errorText}</Text> : null}

      <TextInput
        label="Password"
        returnKeyType="done"
        value={password.value}
        onChangeText={text => setPassword({value: text, error: ''})}
        error={!!password.error}
        errorText={password.error}
        secureTextEntry
      />

      <View style={styles.forgotPassword}>
        <TouchableOpacity onPress={() => navigation.navigate('ForgotPassword')}>
          <Text style={styles.label}>Forgot your password?</Text>
        </TouchableOpacity>
      </View>

      <Button mode="contained" onPress={_onLoginPressed}>
        Login
      </Button>

      <View style={styles.row}>
        <Text style={styles.label}>Don’t have an account ? </Text>
        <TouchableOpacity onPress={() => navigation.navigate('Register')}>
          <Text style={styles.link}>Sign up</Text>
        </TouchableOpacity>
      </View>

      <View style={styles.row}>
        <TouchableOpacity onPress={() => {}}>
          <Image
            style={styles.imageLogin}
            source={require('../assets/img/google.png')}
          />
        </TouchableOpacity>

        <TouchableOpacity onPress={() => {}}>
          <Image
            style={styles.imageLogin}
            source={require('../assets/img/fb.png')}
          />
        </TouchableOpacity>
      </View>
    </BackgroundGradient>
  );
}

//REGISTER SCREEN

function RegisterScreen({navigation}) {
  const [name, setName] = useState({value: '', error: ''});
  const [email, setEmail] = useState({value: '', error: ''});
  const [password, setPassword] = useState({value: '', error: ''});

  const _onSignUpPressed = () => {
    const nameError = nameValidator(name.value);
    const emailError = emailValidator(email.value);
    const passwordError = passwordValidator(password.value);

    if (emailError || passwordError || nameError) {
      setName({...name, error: nameError});
      setEmail({...email, error: emailError});
      setPassword({...password, error: passwordError});
      return;
    }

    navigation.navigate('Login');
  };

  return (
    <BackgroundGradient>
      <TouchableOpacity
        onPress={() => navigation.goBack()}
        style={styles.container}>
        <Image
          style={styles.image}
          source={require('../assets/img/arrow_back.png')}
        />
      </TouchableOpacity>

      <Logo />

      <Header>Create Your Account</Header>

      <TextInput
        label="Name"
        returnKeyType="next"
        value={name.value}
        onChangeText={text => setName({value: text, error: ''})}
        error={!!name.error}
        errorText={name.error}
      />

      <TextInput
        label="Email"
        returnKeyType="next"
        value={email.value}
        onChangeText={text => setEmail({value: text, error: ''})}
        error={!!email.error}
        errorText={email.error}
        autoCapitalize="none"
        autoCompleteType="email"
        textContentType="emailAddress"
        keyboardType="email-address"
      />

      <TextInput
        label="Password"
        returnKeyType="done"
        value={password.value}
        onChangeText={text => setPassword({value: text, error: ''})}
        error={!!password.error}
        errorText={password.error}
        secureTextEntry
      />

      <Button mode="contained" onPress={_onSignUpPressed} style={styles.button}>
        Sign Up
      </Button>

      <View style={styles.row}>
        <Text style={styles.label}>Already have an account? </Text>
        <TouchableOpacity onPress={() => navigation.navigate('Login')}>
          <Text style={styles.link}>Login</Text>
        </TouchableOpacity>
      </View>
    </BackgroundGradient>
  );
}

//FORGOT SCREEN

function ForgotPasswordScreen({navigation}) {
  const [email, setEmail] = useState({value: '', error: ''});

  const _onSendPressed = () => {
    const emailError = emailValidator(email.value);

    if (emailError) {
      setEmail({...email, error: emailError});
      return;
    }

    navigation.navigate('Login');
  };

  return (
    <BackgroundGradient>
      <TouchableOpacity
        onPress={() => navigation.goBack()}
        style={styles.container}>
        <Image
          style={styles.image}
          source={require('../assets/img/arrow_back.png')}
        />
      </TouchableOpacity>

      <Logo />

      <Header>Restore Password</Header>

      <TextInput
        label="E-mail address"
        returnKeyType="done"
        value={email.value}
        onChangeText={text => setEmail({value: text, error: ''})}
        error={!!email.error}
        errorText={email.error}
        autoCapitalize="none"
        autoCompleteType="email"
        textContentType="emailAddress"
        keyboardType="email-address"
      />

      <Button
        mode="contained"
        onPress={_onSendPressed}
        style={styles.buttonForgot}>
        Send Reset Instructions
      </Button>

      <TouchableOpacity
        style={styles.back}
        onPress={() => navigation.navigate('Login')}>
        <Text style={styles.labelForgot}>← Back to login</Text>
      </TouchableOpacity>
    </BackgroundGradient>
  );
}

//TAB BAR DASHBOARD

const Tab = createBottomTabNavigator();

function DashboardTabs() {
  return (
    <Tab.Navigator
      screenOptions={({route}) => ({
        tabBarIcon: ({focused, color, size}) => {
          let iconName;
          if (route.name === 'Home') {
            iconName = require('../assets/img/home.png');
          } else if (route.name === 'Course') {
            iconName = require('../assets/img/course.png');
          } else if (route.name === 'List') {
            iconName = require('../assets/img/list.png');
          } else if (route.name === 'Challange') {
            iconName = require('../assets/img/challange.png');
          }
          // You can return any component that you like here!
          return (
            <Image
              source={iconName}
              style={{width: 20, height: 20, marginTop: 3.5}}
            />
          );
        },
      })}
      tabBarOptions={{
        labelStyle: {fontSize: 15, marginBottom: 3.5},
        tabStyle: {
          maxHeight: 100,
          borderColor: '#414757',
          borderLeftWidth: 0.3,
        },
        activeTintColor: 'white',
        inactiveTintColor: '#212E53',
        activeBackgroundColor: theme.colors.primary,
        inactiveBackgroundColor: 'white',
      }}>
      <Tab.Screen name="Home" component={HomeScreen} />

      <Tab.Screen name="Course" component={CourseScreen} />

      <Tab.Screen name="List" component={ListScreen} />

      <Tab.Screen name="Challange" component={ChallangeScreen} />
    </Tab.Navigator>
  );
}

//DRAWER

const Drawer = createDrawerNavigator();

function AppDraw() {
  return (
    <Drawer.Navigator
      drawerStyle={{
        backgroundColor: theme.colors.surface,
        width: '57%',
      }}
      drawerPosition="right"
      initialRouteName="Home"
      drawerContent={props => <DrawerContent {...props} />}>
      <Drawer.Screen name="Home" component={DashboardTabs} />
      <Drawer.Screen name="Profile" component={ProfileScreen} />
      <Drawer.Screen name="My Event" component={EventSaya} />
      <Drawer.Screen name="My Course" component={CourseSaya} />
      <Drawer.Screen name="My Challange" component={ChallangeSaya} />
      <Drawer.Screen name="Transaksi" component={RiwayatTransaksi} />
      <Drawer.Screen name="Notifikasi" component={Notification} />
    </Drawer.Navigator>
  );
}

const Stack = createStackNavigator();

function StackScreen() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Welcome">
        <Stack.Screen
          name="Welcome"
          component={WelcomeScreen}
          options={{
            headerShown: false,
          }}
        />

        <Stack.Screen
          name="Login"
          component={LoginScreen}
          options={{
            headerShown: false,
          }}
        />

        <Stack.Screen
          name="Register"
          component={RegisterScreen}
          options={{
            headerShown: false,
          }}
        />

        <Stack.Screen
          name="ForgotPassword"
          component={ForgotPasswordScreen}
          options={{
            headerShown: false,
          }}
        />

        <Stack.Screen
          name="Dashboard"
          component={AppDraw}
          options={{
            headerShown: false,
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default StackScreen;

const styles = StyleSheet.create({
  label: {
    color: theme.colors.secondary,
  },
  input: {
    backgroundColor: theme.colors.surface,
  },
  labelForgot: {
    color: theme.colors.secondary,
    width: '100%',
    fontSize: 14,
  },
  button: {
    marginTop: 24,
  },
  buttonForgot: {
    marginTop: 12,
  },
  row: {
    flexDirection: 'row',
    marginTop: 4,
  },
  link: {
    fontWeight: 'bold',
    color: theme.colors.primary,
  },
  forgotPassword: {
    width: '100%',
    alignItems: 'flex-end',
    marginBottom: 24,
  },
  back: {
    width: '100%',
    marginTop: 12,
  },
  container: {
    position: 'absolute',
    top: 2 + getStatusBarHeight(),
    left: 5,
  },
  image: {
    width: 24,
    height: 24,
  },
  imageLogin: {
    width: 35,
    height: 35,
    marginTop: 15,
    marginHorizontal: 20,
  },
});
